//
//  ViewController.swift
//  wmsExample
//
//  Created by Jamia Purevdorj on 6/30/16.
//  Copyright © 2016 Mongol ID Group. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = "https://api.minu.mn/wms/?service=WMS&version=1.3.0&request=GetMap&layers=mongolid:mimap_mobile&styles=&srs=EPSG:3857&format=image/jpeg&TRANSPARENT=true&TILED=true&WIDTH=256&HEIGHT=256&bbox="
        
        let overlay = WMSTileOverlay(url: url, useMercator: true)
        overlay.canReplaceMapContent = true
        mapView.addOverlay(overlay, level: .AboveLabels)
        let region: MKCoordinateRegion = MKCoordinateRegionMake(CLLocationCoordinate2DMake(47.917345, 106.916971), MKCoordinateSpanMake(0.03, 0.03))
        mapView.setRegion(region, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        guard let tileOverlay = overlay as? MKTileOverlay else {
            return MKOverlayRenderer()
        }
        //MapKit дээр нь MAP.MINU.MN-р overlay хийнэ
        return MKTileOverlayRenderer(tileOverlay: tileOverlay)
    }
    


}

