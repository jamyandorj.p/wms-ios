//
//  WMSTileOverlay.h
//  wmsExample
//
//  Created by Jamia Purevdorj on 6/30/16.
//  Copyright © 2016 Mongol ID Group. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface WMSTileOverlay : MKTileOverlay

@property (nonatomic, strong) NSString * url;
@property (nonatomic, assign) BOOL       useMercator; // -- if use 900913 mercator projection or WGS84

- (id)initWithUrl:(NSString*)urlArg  UseMercator: (BOOL) useMercatorArg;
@end



